#include <iostream>
#include <ctime>
#include <vld.h>

#include "Array.h"
#include "ArrayIterator.h"
#include "StoredDisk.h"
#include "StoredUSB.h"
#include "ImpFATFile.h"
#include "ImpNTFSFile.h"

enum Answer {
	ITERATOR,
	BRIDGE, 
	NOTHING
};

Answer QuestionUser(void) {
	std::cout << "Choice pattern:" << std::endl;
	std::cout << "1 - ITERATOR" << std::endl;
	std::cout << "2 - BRIDGE" << std::endl;

	int answer = 0;
	std::cin >> answer;

	switch (answer){
	case 1: return ITERATOR;
	case 2: return BRIDGE;
	}

	return NOTHING;
}

void IteratornPattern() {
	srand(time(nullptr));

	const int count		= 10;
	const int min_elem	= -20;
	const int max_elem	= 50;

	Array dataArray;
	for (size_t i = 0; i < count; ++i) {
		dataArray.AppendItem(min_elem + rand() % (max_elem - min_elem + 1));
	}

	ArrayIterator *it = (ArrayIterator *)dataArray.CreateIterator();
	size_t i = 0;

	std::cout << "Random array:" << std::endl;
	for (it->First(); !it->IsDone(); it->Next()) {
		std::cout << "[" << i << "]" << "\t" << it->CurrentItem() << std::endl;
		i++;
	}

	delete it;

	dataArray.RemoveItem(5);
	it = (ArrayIterator *)dataArray.CreateIterator();
	i = 0;

	std::cout << "Remove element with index 5:" << std::endl;
	std::cout << "Random array:" << std::endl;
	for (it->First(); !it->IsDone(); it->Next()) {
		std::cout << "[" << i << "]" << "\t" << it->CurrentItem() << std::endl;
		i++;
	}
}

void BridgePattern() {
	StoredUSB usbFile(new ImpFATFile());
	StoredDisk diskFile(new ImpNTFSFile());

	std::cout << "Processing usb file: " << std::endl;
	usbFile.ProcessFile();

	std::cout << "\nProcessing disk file: " << std::endl;
	diskFile.ProcessFile();
}

int main(void) {

	Answer answer;
	while ((answer = QuestionUser()) == NOTHING);

	switch (answer) {
	case ITERATOR:
		IteratornPattern();
		break;
	case BRIDGE:
		BridgePattern();
		break;
	}

	return 0;
}
	