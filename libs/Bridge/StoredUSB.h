#pragma once
#include "AbstractFile.h"
class StoredUSB :public AbstractFile{
public:
	StoredUSB(ImplementorFile* imp) :AbstractFile() {
		this->file = imp;
	}
	~StoredUSB() {}

	virtual void ProcessFile();
};

