#pragma once

class ImplementorFile {
protected:
	virtual ~ImplementorFile() {}
public:
	virtual void OpenFile()		= 0;
	virtual void CloseFile()	= 0;
	virtual void ReadFile()		= 0;
	virtual void WriteFile()	= 0;
};