#include <iostream>
#include <string>
#include "ImpNTFSFile.h"

void ImpNTFSFile::OpenFile() {
	std::cout << "Open file in NTFS system" << std::endl;
}
void ImpNTFSFile::CloseFile() {
	std::cout << "Close file in NTFS system" << std::endl;
}
void ImpNTFSFile::ReadFile() {
	std::cout << "Read file in NTFS system" << std::endl;
}
void ImpNTFSFile::WriteFile() {
	std::cout << "Write file in NTFS system" << std::endl;
}
