#pragma once
#include "AbstractFile.h"

class StoredDisk :public AbstractFile {
public:
	StoredDisk(ImplementorFile* imp) :AbstractFile() {
		this->file = imp;
	}

	~StoredDisk() {};

	virtual void ProcessFile();
};

