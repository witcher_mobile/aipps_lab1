#pragma once
#include "ImplementorFile.h"
class ImpNTFSFile :public ImplementorFile
{
public:
	ImpNTFSFile() :ImplementorFile() {}
	~ImpNTFSFile() {}

	virtual void OpenFile();
	virtual void CloseFile();
	virtual void ReadFile();
	virtual void WriteFile();
};

