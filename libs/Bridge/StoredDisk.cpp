#include "StoredDisk.h"

void StoredDisk::ProcessFile() {
	this->file->OpenFile();
	this->file->WriteFile();
	this->file->ReadFile();
	this->file->CloseFile();
}