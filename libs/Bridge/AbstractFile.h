#pragma once
#include "ImplementorFile.h"

class AbstractFile {
protected:
	ImplementorFile *file;

	virtual ~AbstractFile() {}
public:
	virtual void ProcessFile() = 0;
};
