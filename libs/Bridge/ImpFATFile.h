#pragma once
#include "ImplementorFile.h"

class ImpFATFile :public ImplementorFile
{
public:
	ImpFATFile() :ImplementorFile() {}
	~ImpFATFile() {}

	virtual void OpenFile();
	virtual void CloseFile();
	virtual void ReadFile();
	virtual void WriteFile();
};

