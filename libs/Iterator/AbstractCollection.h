#pragma once

#include "Iterator.h"

template < class Item >
class AbstractCollection{
protected:
	virtual ~AbstractCollection() {};
public:
	virtual void AppendItem(const Item& item)		= 0;
	virtual bool RemoveItem(const Item& item)		= 0;
	virtual bool RemoveItem(int index)				= 0;
	virtual Item GetItem(int index)	const			= 0;
	virtual int FindItem(const Item& item) const	= 0;
	virtual size_t CountItem(void) const			= 0;
	virtual Iterator < Item >* CreateIterator(void)	= 0;
};

