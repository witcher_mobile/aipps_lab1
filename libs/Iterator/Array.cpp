#include "LinkArray.h"
#include <new>
#include <stdexcept>

Array::Array(void):AbstractCollection < double >(){
	this->data = nullptr;
	this->size = 0;
}

Array::Array(double* data, size_t size) :AbstractCollection < double >() {
	if (this->size == 0 || this->data == nullptr) {
		throw std::bad_alloc();
	}

	this->size = size;
	this->data = new double[this->size];

	for (size_t i = 0; i < this->size; ++i) {
		this->data[i] = data[i];
	}
}

Array::~Array(){
	if (this->data != nullptr)
		delete[] this->data;
}

size_t Array::CountItem(void) const {
	return this->size;
}

void Array::AppendItem(const double& item) {
	if (this->size == 0) {
		this->size++;
		this->data = new double[this->size];

		this->data[0] = item;
		return;
	}

	double *temp = new double[this->size + 1];
	for (size_t i = 0; i < this->size; ++i) {
		temp[i] = this->data[i];
	}

	temp[this->size] = item;
	this->size++;
	
	delete[] this->data;
	this->data = temp;
}

bool Array::RemoveItem(const double& item) {
	int index = this->FindItem(item);
	if (index == -1) return false;
	return this->RemoveItem(index);
}

bool Array::RemoveItem(int index) {
	if (index < 0 || index >= this->size || this->size == 0) 
		return false;

	if (this->size == 1) {
		this->size = 0;
		delete[] this->data;
		this->data = nullptr;
	}

	double *temp = new double[this->size - 1];

	for (size_t i = 0; i < index; ++i)
		temp[i] = this->data[i];
	for (size_t i = index; i < this->size - 1; ++i)
		temp[i] = this->data[i + 1];


	this->size--;
	delete[] this->data;
	this->data = temp;
}

double Array::GetItem(int index) const {
	if (index < 0 || index >= this->size || this->size == 0) {
		throw std::out_of_range("Error index");
	}

	return this->data[index];
}

int Array::FindItem(const double& item) const {
	int index = -1;

	for (size_t i = 0; i < this->size; ++i) {
		if (this->data[i] == item) {
			index = i;
			break;
		}
	}

	return index;
}

Iterator < double >* Array::CreateIterator(void) {
	return (Iterator < double >*) new ArrayIterator(this);
}
