#include "ArrayIterator.h"
#include <stdexcept>

ArrayIterator::ArrayIterator(const Array* array) :Iterator < double >(){
	this->array = array;
	this->currentItem = 0;
}

ArrayIterator::~ArrayIterator(){
	this->array = nullptr;
}

void ArrayIterator::First(void) {
	this->currentItem = 0;
}
void ArrayIterator::Next(void) {
	this->currentItem++;
}
bool ArrayIterator::IsDone(void) const {
	return this->currentItem >= this->array->CountItem();
}

double ArrayIterator::CurrentItem(void) const {
	if (this->IsDone()) {
		throw std::out_of_range("Error iterator");
	}

	return this->array->GetItem(this->currentItem);
}
