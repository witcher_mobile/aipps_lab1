#pragma once
#include "AbstractCollection.h"

class Array :public AbstractCollection < double >{
private:
	double *data;
	size_t size;

public:
	Array(void);
	Array(double* data, size_t size);
	~Array(void);

	virtual size_t CountItem(void) const;
	virtual void AppendItem(const double& item);
	virtual bool RemoveItem(const double& item);
	virtual bool RemoveItem(int index);
	virtual double GetItem(int index) const;
	virtual int FindItem(const double& item) const;
	virtual Iterator < double >* CreateIterator(void);
};

