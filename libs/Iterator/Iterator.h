#pragma once

template < class Item >
class Iterator {
protected:
	virtual ~Iterator(void) {};
public:
	virtual void First(void)				= 0;
	virtual void Next(void)					= 0;
	virtual bool IsDone(void) const			= 0;
	virtual Item CurrentItem(void) const	= 0;
};