#pragma once

#include "AbstractCollection.h"
#include "LinkArray.h"

class ArrayIterator :Iterator < double >{
private:
	size_t currentItem;
	const Array *array;
public:
	ArrayIterator(const Array* array);
	~ArrayIterator();

	virtual void First(void);
	virtual void Next(void);
	virtual bool IsDone(void) const;
	virtual double CurrentItem(void) const;
};

